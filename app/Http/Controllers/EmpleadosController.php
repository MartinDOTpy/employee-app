<?php

namespace App\Http\Controllers;

use App\Models\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class EmpleadosController extends Controller
{
    public function index(){

        $datos['empleados'] = Empleado::paginate(5);

        return view('empleados.index', $datos);
    }

    public function create(){
        return view('empleados.create');
    }

    public function store(Request $request){

        $datosEmpleado = request()->except('_token');

        if($request->hasFile('CredencialElector')){
            
            $datosEmpleado['CredencialElector'] = $request->file('CredencialElector')->store('uploads', 'public');
        }

        Empleado::insert($datosEmpleado);

        return redirect('empleados');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
        $empleado = Empleado::findOrFail($id);
        return view('empleados.edit', compact('empleado'));
    }

    public function update(Request $request, $id)
    {
        $empleado = Empleado::findOrFail($id);
        $datosEmpleado = request()->except(['_token', '_method']);

        File::delete(storage_path().'/app/public/'.$empleado->CredencialElector);
        if($request->hasFile('CredencialElector')){
            
            $datosEmpleado['CredencialElector'] = $request->file('CredencialElector')->store('uploads', 'public');
        }

        Empleado::where('id', '=', $id)->update($datosEmpleado);
        // $empleado = Empleado::findOrFail($id);
        // return view('empleados.edit', compact('empleado'));
        return redirect('/empleados');
    }

    public function destroy($id){
        $empleado = Empleado::findOrFail($id);

        Empleado::destroy($id);
        File::delete(storage_path().'/app/public/'.$empleado->CredencialElector);

        return redirect('/empleados');
    }
}