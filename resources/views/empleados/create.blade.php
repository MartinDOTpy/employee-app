@extends('empleados.layout')
 
@section('title', 'Crear')
 
@section('content')
<h1 style="font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif">Agregar Empleados</h1>
<form action="{{ url('/empleados') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    
    <label for="Nombre">{{'Nombre'}}</label>
    <input class="form-control" type="text" name="Nombre" id="Nombre" value="">
    <br>
    
    <label for="Domicilio">{{'Domicilio'}}</label>
    <input class="form-control" type="text" name="Domicilio" id="Domicilio" value="">
    <br>
    
    <label for="Folio">{{'Folio'}}</label>
    <input class="form-control" type="number" name="Folio" id="Folio" value="">
    <br>
    
    <label for="CredencialElector">{{'CredencialElector'}}</label>
    <input class="form-control" type="file" name="CredencialElector" id="CredencialElector" value="">
    <br>

    <input class="btn btn-success" type="submit" value="Agregar">
    <a class="btn btn-secondary" href="{{ url('/empleados') }}">Volver</a>
</form>
@endsection