@extends('empleados.layout')
 
@section('title', 'Indice')
 
@section('content')
<h1 style="font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif">Administrador de Empleados</h1>
<a class="btn btn-success mb-4" href="{{ url('empleados/create') }}">Agregar empleado</a>

<table class="table table-striped table-light">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Domicilio</th>
            <th scope="col">No. de Folio</th>
            <th scope="col">Credencial Elector</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody class="tbody-light">
    @foreach($empleados as $empleado)
        <tr>
            <td scope="row">{{ $loop->iteration }}</td>
            <td>{{ $empleado -> Nombre }}</td>
            <td>{{ $empleado -> Domicilio }}</td>
            <td>{{ $empleado -> Folio }}</td>
            <td><a href="{{ asset('storage').'/'.$empleado->CredencialElector}}" download>Descargar</a></td>
            <td>
                <div class="row w-75">
                    <div class="col">
                        <a class="btn btn-warning" href="{{ url('empleados/'.$empleado->id.'/edit') }}">Editar</a>
                    </div>
                    <div class="col">
                        <form action="{{ url('/empleados/'.$empleado->id) }}" method="post">
                            {{csrf_field() }}
                            {{method_field('DELETE') }}
                            <button class="btn btn-danger" onclick="return confirm('Seguro que desea borrar este empleado?');" type="submit">Borrar</button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection