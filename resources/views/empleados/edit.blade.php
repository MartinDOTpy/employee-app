@extends('empleados.layout')
 
@section('title', 'Editar')
 
@section('content')
<h1 style="font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif">Editar Empleados</h1>
<form action="{{ url('/empleados/'.$empleado->id) }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}

    <label for="Nombre">{{'Nombre'}}</label>
    <input class="form-control" type="text" name="Nombre" id="Nombre" value="{{ $empleado->Nombre }}">
    <br>
    
    <label for="Domicilio">{{'Domicilio'}}</label>
    <input class="form-control" type="text" name="Domicilio" id="Domicilio" value="{{ $empleado->Domicilio }}">
    <br>
    
    <label for="Folio">{{'Folio'}}</label>
    <input class="form-control" type="number" name="Folio" id="Folio" value="{{ $empleado->Folio }}">
    <br>
    
    <label for="CredencialElector">{{'CredencialElector'}}</label>
    <a href="{{ asset('storage').'/'.$empleado->CredencialElector}}" download>Archivo cargado</a>
    <input class="form-control" type="file" name="CredencialElector" id="CredencialElector" value="">
    <br>

    <input class="btn btn-warning" type="submit" value="Editar">
    <a class="btn btn-secondary" href="{{ url('/empleados') }}">Volver</a>
</form>
@endsection